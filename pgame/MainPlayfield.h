#pragma once
#include "GameEventAbstract.h"
#include "Player.h"
#include "Snowman.h"

class MainPlayfield :
	public GameEventAbstract
{
public:
	Player player;
	Snowman enemy1;

	MainPlayfield();
	virtual void Update(sf::Time time);
	virtual void Init();
	virtual void PollKeys(sf::Time time);
	virtual void Draw(sf::RenderWindow &win);

	virtual void KeyPressed(sf::Event & e, sf::Time time);
	virtual void KeyReleased(sf::Event & e, sf::Time time);

	~MainPlayfield();
};

