#pragma once
#include <SFML/Graphics.hpp>
class GameObject
{


public:
	sf::Vector2f position;
	sf::Vector2f acceleration;
	bool alive;

	GameObject();
	virtual ~GameObject();
};

