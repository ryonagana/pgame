#include "GameEventList.h"



void GameEventList::Add(GameEventAbstract & event)
{
	eventList.push_back(&event);


}

const std::vector<GameEventAbstract*> GameEventList::getList() const
{
	return eventList;
}

GameEventAbstract * GameEventList::getEvent()
{
	if (eventList.size() > 0) {
		return eventList.back();
	}
	return nullptr;
}

void GameEventList::Go(std::size_t index)
{
	if (eventList.size() > 0) {
		eventList.back()->Pause();
		eventList[index]->Play();
		return;
	}
}

void GameEventList::Pause()
{
	if (eventList.size() > 0) {
		eventList.back()->Pause();
		return;
	}
}

void GameEventList::Stop()
{
	if (eventList.size() > 0) {
		eventList.back()->Stop();
		return;
	}
}

void GameEventList::Update(sf::Time time)
{
	if (eventList.size() > 0) {
		eventList.back()->Update(time);
		return;
	}
	
}

void GameEventList::Draw(sf::RenderWindow & win)
{
	if (eventList.size() > 0) {
		eventList.back()->Draw(win);
		return;
	}
}

void GameEventList::Clear()
{
	std::vector<GameEventAbstract*>::iterator it;
	for (it = eventList.begin(); it != eventList.end(); ++it) {
		auto ev = *it;
		if (ev != nullptr) {
			ev->Destroy();
		}

	}
}

GameEventList::GameEventList()
{
}


GameEventList::~GameEventList()
{
}
