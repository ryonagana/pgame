#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>


class GameUtils
{
public:

	static const int SCREEN_WIDTH = 800;
	static const int SCREEN_HEIGHT = 600;


	static sf::Texture* LoadSprite(const std::string &filename) {
		sf::Texture *tex = new sf::Texture();


		if (!tex->loadFromFile(filename)) {
			fprintf(stderr, "GameUtils.cpp: Missing %s", filename);
			
			tex->create(32, 32);
			sf::Uint8* pixels = new sf::Uint8[tex->getSize().x * tex->getSize().y * 4];
			tex->update(pixels);
			
			return tex;

		}

	
		return tex;

	}

	static sf::Texture* LoadTextureColorMask(const std::string &filename, sf::Color mask) {
		sf::Texture *tex = GameUtils::LoadSprite(filename);
		sf::Image img;

		img = tex->copyToImage();
		img.createMaskFromColor(mask);
		tex->loadFromImage(img);

		return tex;

	
	}

	GameUtils();
	~GameUtils();
};

