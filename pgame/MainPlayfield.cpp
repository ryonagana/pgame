#include "MainPlayfield.h"



MainPlayfield::MainPlayfield() : GameEventAbstract()
{
	enemy1.animatedObject.setPosition(sf::Vector2f(.0f,.0f));
}

void MainPlayfield::Update(sf::Time time)
{
	player.Update(time);
	enemy1.Think(time, &player);
	enemy1.Update(time);
}

void MainPlayfield::Init()
{
	
}

void MainPlayfield::PollKeys(sf::Time time)
{
	

}

void MainPlayfield::Draw(sf::RenderWindow & win)
{
	win.draw(player.animatedObj);
	win.draw(enemy1.animatedObject);
}

void MainPlayfield::KeyPressed(sf::Event & e, sf::Time  time)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		player.acceleration.y = 1.0f;
		player.MoveLeft(time);

	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		player.acceleration.y = 1.0f;
		player.MoveRight(time);
	}
}

void MainPlayfield::KeyReleased(sf::Event & e, sf::Time  time)
{
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		player.acceleration.y = 0;
		player.MoveLeft(time);

	}

	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		player.acceleration.y = 0;
		player.MoveRight(time);

	}

	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		player.MoveUp(time);
	}
	
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		player.MoveDown(time);
	}

}


MainPlayfield::~MainPlayfield()
{
}
