#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window/Keyboard.hpp>
#include "GameEventList.h"
#include "GameUtils.h"
#include "MainPlayfield.h"

int main(void) {

	sf::RenderWindow window(sf::VideoMode(GameUtils::SCREEN_WIDTH, GameUtils::SCREEN_HEIGHT), "SKIPlatform Game?");
	
	window.setFramerateLimit(60);
	window.setVerticalSyncEnabled(true);
	window.setActive(true);

	sf::Vector2u size = window.getSize();
	sf::Clock mainClock;
	GameEventList events;

	MainPlayfield *m = new MainPlayfield();

	events.Add(*m);




	while (window.isOpen()) {

		sf::Event event;

		while (window.pollEvent(event))
		{
		

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || event.type == sf::Event::Closed) {
				window.close();
			}

			if (event.type == sf::Event::KeyPressed) {
				m->KeyPressed(event, mainClock.getElapsedTime());
			}

			if (event.type == sf::Event::KeyReleased) {
				m->KeyReleased(event, mainClock.getElapsedTime());
			}


			
		}



		sf::Time frameTime = mainClock.restart();
		events.Update(frameTime);


		window.clear(sf::Color::White);
		events.Draw(window);
		window.display();

	}


	return 0;
}
