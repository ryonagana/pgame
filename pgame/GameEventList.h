#pragma once
#include <iostream>
#include <vector>
#include "GameEventAbstract.h"

class GameEventList
{
private:
	std::vector<GameEventAbstract*> eventList;
public:
	
	void Add(GameEventAbstract &event);
	const std::vector<GameEventAbstract*> getList() const;
	GameEventAbstract* getEvent();
	void Go(std::size_t index);
	void Pause();
	void Stop();
	void Update(sf::Time time);
	void Draw(sf::RenderWindow &win);
	void Clear();
	GameEventList();
	~GameEventList();
};

