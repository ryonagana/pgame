#include "AnimatedObject.h"

AnimatedObject::AnimatedObject(sf::Time frameTime, bool loop, bool pause) {
	isLooped = loop;
	isPaused = pause;
	currentFrame = 0;

}

void AnimatedObject::setAnimation(Animation &anim) {
	pAnimation = &anim;
	pTexture = &(*anim.getSheet());
	this->setTexture(*pAnimation->getSheet());
	this->setTextureRect(pAnimation->getFrame(currentFrame));
	setFrame(currentFrame);
}

Animation* AnimatedObject::getAnimation()
{
	return pAnimation;
}

void AnimatedObject::Update(sf::Time deltaTime)
{
	if (pAnimation && !isPaused) {

		currentTime += deltaTime;

		if (currentTime.asSeconds() >= frameTime.asSeconds() ) {

			currentTime = sf::microseconds(currentTime.asMicroseconds() % frameTime.asMicroseconds());

			if (currentFrame + 1 < pAnimation->getSize()) {
				currentFrame++;
			}
			else {
				currentFrame = 0;

				if (!isLooped) {
					isPaused = true;
				}
			}

			setFrame(currentFrame, false);
			
		}
		
	}
}

void AnimatedObject::setFrameTime(sf::Time time)
{
	frameTime = time;
}

sf::Time AnimatedObject::getFrameTime()
{
	return frameTime;
}

sf::Time AnimatedObject::getCurrenTime()
{
	return currentTime;
}

void AnimatedObject::Play()
{
}

void AnimatedObject::Play(Animation & anim)
{
	isPaused = false;
	if (getAnimation() != &anim) {
		setAnimation(anim);
	}

	
}

void AnimatedObject::Stop()
{
	isPaused = true;
	isLooped = false;
}

void AnimatedObject::Pause()
{
	isPaused = true;
}

void AnimatedObject::setFrame(std::size_t newFrame, bool reset)
{
	if (pAnimation) {

		sf::IntRect r = pAnimation->getFrame(newFrame);
		setTextureRect(r);

		if (reset) {
			currentTime = sf::Time::Zero;
		}
	}
}
