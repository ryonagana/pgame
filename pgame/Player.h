#pragma once
#include "GameObject.h"
#include <SFML/Graphics.hpp>
#include "AnimatedObject.h"


class Player :
	public GameObject
{
private:
	bool playerKeys[10];


public:

	static const int KEY_LEFT = 0;
	static const int KEY_RIGHT = 1;
	static const int KEY_UP = 2;
	static const int KEY_DOWN = 3;
	static const int KEY_ACTION = 4;

	bool startScrolling;


	Animation *currentAnimation;
	sf::Sprite playerSprite;
	sf::Texture playerTexture;
	sf::Image   playerImage;
	Animation anim_idle_state;
	AnimatedObject animatedObj;
	bool isPlayerSliding;


	Player();
	sf::Sprite GetSprite();

	void MoveLeft(sf::Time &time);
	void MoveRight(sf::Time &time);
	void MoveUp(sf::Time &time);
	void MoveDown(sf::Time &time);

	void KeyPress(int key);
	void KeyRelease(int key);

	bool getKey(int key);

	void SpawnPos(float x, float y);
	void SpawnCenterPos(sf::RenderWindow &win);

	void UpdateMovement();

	void Update(sf::Time time);
	virtual ~Player();

};

