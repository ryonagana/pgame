#pragma once
#include "Enemy.h"
class Snowman :
	public Enemy
{
public:
	float angle;
	Snowman();
	virtual void Think(sf::Time time, Player *player);
	virtual void Update(sf::Time time);
	~Snowman();
};

