#pragma once
#include <iostream>
#include <functional>
#include "GameObject.h"
#include "Player.h"
#include "AnimatedObject.h"



class Enemy :
	public GameObject
{
public:
	static const int ENEMY_STATE_IDLE = 0;
	static const int ENEMY_STATE_WALKING = 1;
	static const int ENEMY_STATE_PLAYER_SEEN = 2;
	static const int ENEMY_STATE_STOPPED = 3;
	static const int ENEMY_STATE_INVENCIBLE = 1 << 8;

	Animation anim_idle;
	Animation anim_walking;
	Animation *currentAnimation;
	AnimatedObject animatedObject;
	sf::Time thinkSpeed;
	sf::Time currentTime;

	virtual void Think(sf::Time time, Player *player) 
	{

	}
	virtual void Draw(sf::RenderWindow &win) {

	}

	virtual void Update(sf::Time time)
	{
		if (currentAnimation != nullptr) {
			animatedObject.setAnimation(*currentAnimation);
			animatedObject.Play(*currentAnimation);
		}

		animatedObject.move(position);
	}

	virtual bool isCollide(GameObject *obj) {
	
		if ( (dynamic_cast<Player*>(obj)) != nullptr) {
			Player *p = dynamic_cast<Player*>(obj);
			return p->animatedObj.getGlobalBounds().intersects(animatedObject.getGlobalBounds());
		}

		if ((dynamic_cast<Enemy*>(obj)) != nullptr) {
			Enemy *p = dynamic_cast<Enemy*>(obj);
			return p->animatedObject.getGlobalBounds().intersects(animatedObject.getGlobalBounds());
		}

		return false;

		
	}



	Enemy(sf::Time updatespeed = sf::seconds(0.7));
	~Enemy();
};

