#include "Animation.h"



void Animation::add(sf::IntRect rect)
{
	m_frames.push_back(rect);
}

void Animation::setSheet(sf::Texture & tex)
{
	m_texture = &tex;
}

sf::Texture * Animation::getSheet() const
{
	return m_texture;
}

std::size_t Animation::getSize() const
{
	return m_frames.size();
}

void Animation::setRectLeft(std::size_t index, int left)
{
	m_frames[index].left = left;
	
}

void Animation::setRectTop(std::size_t index, int top)
{
	m_frames[index].top = top;

}

const sf::IntRect & Animation::getFrame(std::size_t index) const
{
	return  m_frames[index];
}

Animation::Animation()
{
}

