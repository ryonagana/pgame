#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/System/Time.hpp>

class IGameEvent
{
public:
	virtual void Init() = 0;
	virtual void Play() = 0;
	virtual void Pause() = 0;
	virtual void Stop() = 0;
	virtual void Destroy() = 0;

	virtual void Update(sf::Time) = 0;
	virtual void PollKeys(sf::Time time) = 0;
	virtual void KeyPressed(sf::Event &e, sf::Time &time) = 0;
	virtual void KeyReleased(sf::Event &e, sf::Time &time) = 0;
	virtual void Draw(sf::RenderWindow &win) = 0;

};

