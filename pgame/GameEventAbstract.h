#pragma once
#include "IGameEvent.h"
class GameEventAbstract :
	public IGameEvent
{
protected:
	bool isPaused;
	
public:
	GameEventAbstract();
	~GameEventAbstract();

	// Herdado por meio de IGameEvent
	virtual void Init() override;
	virtual void Play() override;
	virtual void Pause() override;
	virtual void Stop() override;
	virtual void Destroy() override;
	virtual void Update(sf::Time) override;
	virtual void Draw(sf::RenderWindow & win) override;
	virtual void PollKeys(sf::Time time) override;

	// Herdado por meio de IGameEvent
	virtual void KeyPressed(sf::Event & e, sf::Time & time) override;
	virtual void KeyReleased(sf::Event & e, sf::Time & time) override;
};

