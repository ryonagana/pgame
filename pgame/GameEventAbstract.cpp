#include "GameEventAbstract.h"



GameEventAbstract::GameEventAbstract()
{
	Init();
}


GameEventAbstract::~GameEventAbstract()
{
}

void GameEventAbstract::Init()
{

}

void GameEventAbstract::Play()
{

}

void GameEventAbstract::Pause()
{

}

void GameEventAbstract::Stop()
{
}

void GameEventAbstract::Destroy()
{
}

void GameEventAbstract::Update(sf::Time)
{
}

void GameEventAbstract::Draw(sf::RenderWindow & win)
{
	
}

void GameEventAbstract::PollKeys(sf::Time time)
{
}

void GameEventAbstract::KeyPressed(sf::Event & e, sf::Time & time)
{
}

void GameEventAbstract::KeyReleased(sf::Event & e, sf::Time & time)
{
}
