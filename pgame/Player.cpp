#include "Player.h"
#include "GameUtils.h"



Player::Player() :
	GameObject()
{

	sf::Texture *spritesheet_tex = GameUtils::LoadTextureColorMask("media//sprites//ski1.bmp", sf::Color::Magenta);
	
	position = sf::Vector2f(0.0f, 0.0f);
	acceleration = sf::Vector2f(0.0f, 0.0f);

	playerKeys[10] = { false };
	isPlayerSliding = false;
	acceleration.y = 3;
	acceleration.x = 1;
	startScrolling = false;

	anim_idle_state.setSheet(*spritesheet_tex);
	anim_idle_state.add(sf::IntRect(0, 0, 32, 32));
	//anim_idle_state.add(sf::IntRect(64, 0, 32, 32));
	//anim_idle_state.add(sf::IntRect(96, 0, 32, 32));
	//anim_idle_state.add(sf::IntRect(128, 0, 32, 32));
	
	currentAnimation = &anim_idle_state;

	animatedObj.setAnimation(*currentAnimation);
	animatedObj.setFrameTime(sf::seconds(0.1f));
	
	animatedObj.setPosition(sf::Vector2f(GameUtils::SCREEN_WIDTH / 4, GameUtils::SCREEN_HEIGHT / 3));

}

sf::Sprite Player::GetSprite()
{
	return playerSprite;
}

void Player::MoveLeft(sf::Time &time)
{
	position.x -= acceleration.x * time.asSeconds();
	
}

void Player::MoveRight(sf::Time &time)
{
	position.x += acceleration.x * time.asSeconds();
}

void Player::MoveUp(sf::Time &time)
{
	
	position.y -= 1 * time.asSeconds();
	isPlayerSliding = false;
}

void Player::MoveDown(sf::Time &time)
{
	isPlayerSliding = true;
}

void Player::KeyPress(int key)
{
	playerKeys[key] = true;
}

void Player::KeyRelease(int key)
{
	playerKeys[key] = false;
}

bool Player::getKey(int key)
{
	return playerKeys[key];
}

void Player::SpawnPos(float x, float y)
{
	playerSprite.setPosition(sf::Vector2f(x, y));
}

void Player::SpawnCenterPos(sf::RenderWindow &win)
{
	int w, h;
	float ratio;

	w = (win.getPosition().x + playerSprite.getScale().x) / 2;
	h = (win.getPosition().y + playerSprite.getScale().y) / 2;

	ratio = w / h;

	if (ratio > 1.0) {
		playerSprite.setPosition(sf::Vector2f(w, h));
		return;
	}

	return;
}

void Player::UpdateMovement()
{ 
	animatedObj.move(position);
}

void Player::Update(sf::Time time)
{
	
	//fprintf(stdout, "is Scrolled: %s\n\n", startScrolling ? "True" : "False");
	//fprintf(stdout, "Position  %.2f x %.2f\n\n", animatedObj.getPosition().x, animatedObj.getPosition().y);
	
	if (!startScrolling && animatedObj.getPosition().y < (GameUtils::SCREEN_HEIGHT / 2) ) {
		position.y += acceleration.y * time.asSeconds();

	} else {
		startScrolling = true;
		isPlayerSliding = true;
		acceleration.y = 0;
		position.y = 0;
	}

	
	animatedObj.Play(*currentAnimation);
	animatedObj.Update(time);
	UpdateMovement();
}


Player::~Player()
{

	
}



