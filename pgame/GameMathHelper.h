#pragma once
#include <iostream>
#include <SFML/System/Vector2.hpp>
#include <cmath>

#define POW2(v) ((v)*(v))

class GameMathHelper
{
public:

	template<typename T> 
	static T abs(T const &x) {
		return (x < 0) ? -x : x;
	}

	template<typename T>
	static float length(sf::Vector2<T> &vec) {
		return std::sqrt((vec.x * vec.x) + (vec.y * vec.y));
	}

	template<typename T>
	static double magnitude(sf::Vector2<T> &vec) {

		double mag = std::sqrt(POW2(vec.x) + POW2(vec.y));
		return mag;
	}

	template<typename T>
	static sf::Vector2<T>& normalize(sf::Vector2<T> &vec) {
		float len = GameMathHelper::length(vec);
		sf::Vector2f nPos;
		nPos.x /= len;
		nPos.y /= len;
		return nPos;
	}

	GameMathHelper();
	~GameMathHelper();
};

