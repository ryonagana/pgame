#include "Snowman.h"
#include  "GameUtils.h"
#include "GameMathHelper.h"
#include <SFML/Main.hpp>
#include <SFML/Graphics.hpp>
#include <math.h>

Snowman::Snowman() : Enemy()
{
	sf::Texture *tex = GameUtils::LoadTextureColorMask("media//sprites//skiman.bmp", sf::Color::Magenta);
	thinkSpeed = sf::seconds(0.05f);
	anim_idle.setSheet(*tex);
	anim_idle.add(sf::IntRect(0, 64, 32, 64));
	
	anim_walking.setSheet(*tex);
	anim_walking.add(sf::IntRect(0, 64, 32, 64));
	anim_walking.add(sf::IntRect(32, 64, 32, 64));
	anim_walking.add(sf::IntRect(64, 64, 32, 64));

	currentAnimation = &anim_walking;

	animatedObject.setAnimation(*currentAnimation);
	animatedObject.setFrameTime(sf::seconds(0.1f));
	
}

void Snowman::Think(sf::Time time, Player * player)
{
	angle += 1.0f * time.asSeconds();
	currentTime += time;

	/*
	

	sf::Vector2f coord = animatedObject.getPosition() - player->animatedObj.getPosition();
	sf::Vector2f direction = GameMathHelper::normalize<float>(coord);

	sf::Vector2f newPos = sf::Vector2f(sin(angle) * 0.1, cos(angle) * 0.1);
	*/


	if (currentTime.asSeconds() >= thinkSpeed.asSeconds()) {

		float dx = GameMathHelper::abs<float>(player->animatedObj.getGlobalBounds().left - position.x);
		float dy = GameMathHelper::abs<float>(player->animatedObj.getGlobalBounds().top - position.y);
		float distance = std::sqrt((dx * dx) + (dy * dy));

		sf::Vector2f nPos = sf::Vector2f(dx, dy);

		nPos.x /= distance;
		nPos.y /= distance;
		

		animatedObject.move(nPos);

            //fprintf(stdout, "x: %.2f\ny:%.2f\ndist: %.2f\n", nPos.x, nPos.y, distance);
	}

	
}

void Snowman::Update(sf::Time time) 
{
	animatedObject.Play(*currentAnimation);
	animatedObject.Update(time);
}


Snowman::~Snowman()
{
 
}
