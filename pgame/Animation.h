#pragma once

#include <vector>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Texture.hpp>


class Animation
{
private:
	std::vector<sf::IntRect> m_frames;
	sf::Texture *m_texture;

public:
	void add(sf::IntRect rect);
	void setSheet(sf::Texture &tex);
	sf::Texture* getSheet() const;
	std::size_t getSize() const;
	void setRectLeft(std::size_t index, int left);
	void setRectTop(std::size_t index, int top);
	const sf::IntRect& getFrame(std::size_t index) const;

	Animation();
};

