#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/System/Time.hpp>
#include "GameObject.h"
#include "Animation.h"


class AnimatedObject : public sf::Sprite
{
private:
	Animation *pAnimation;
	sf::Texture *pTexture;
	sf::Time frameTime;
	sf::Time currentTime;
	std::size_t  currentFrame;

	bool isLooped;
	bool isPaused;
public:
	AnimatedObject(sf::Time frameTime = sf::seconds(0.2f), bool loop = true, bool pause = false);
	void setAnimation(Animation &anim);
	Animation* getAnimation();

	void Update(sf::Time deltaTime);

	void setFrameTime(sf::Time time);
	sf::Time getFrameTime();
	sf::Time getCurrenTime();

	void Play();
	void Play(Animation &anim);
	void Stop();
	void Pause();


	void setFrame(std::size_t newFrame, bool reset = true);
	
	
};

